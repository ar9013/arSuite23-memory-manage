package com.yue.ar.suite;

import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.utils.Pool;

public class Bullet implements Pool.Poolable {

    // 子彈位置
    public Vector2 position;

    // 子彈是否活著
    public boolean alive;

    public Bullet(){
        this.position = new Vector2();
        this.alive = false;
    }

    public void init(float poX, float posY){
        position.set(poX,posY);

        alive = true;
    }

    @Override
    public void reset() {
        position.set(0,0);
        alive = false;
    }

    public void update(float delta){
        position.add(delta*60,delta*60);

        if(isOutofScreen()){
            alive = false;
        }
    }

    public boolean isOutofScreen() {
        return outofScreen;
    }
}
