package com.yue.ar.suite;

import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.Pool;

public class World {
    private final Array<Bullet>  activeBullets = new Array<Bullet>();

    private final Pool<Bullet> bulletPool = new Pool<Bullet>() {
        @Override
        protected Bullet newObject() {
            return new Bullet();
        }
    };

    public void update(float delta){
        Bullet item = bulletPool.obtain();

        item.init(2,2);
        activeBullets.add(item);

        int len = activeBullets.size;
        for(int i = 0 ; --i >=0;){
            item = activeBullets.get(i);

            bulletPool.free(item);
        }
    }
}
